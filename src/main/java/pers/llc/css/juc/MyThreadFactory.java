package pers.llc.css.juc;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * 给线程命名，查看创建线程数
 * 给线程设置是否是后台运行
 * 设置线程优先级
 *
 * @author lls
 * @time 2018-10-02
 */
public class MyThreadFactory implements ThreadFactory {

    private int counter;
    private String name;
    private List<String> stats;

    public MyThreadFactory(String name) {
        counter = 0;
        this.name = name;
        stats = new ArrayList<>();
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r, name + "-Thread_" + counter);
        counter++;
        stats.add("Created thread " + t.getId() + " with name " + t.getName() + " on " + new Date());
        return t;
    }

    public String getStatus() {
        StringBuffer sb = new StringBuffer();
        Iterator<String> it = stats.iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            sb.append("\n");
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        MyThreadFactory factoryTest = new MyThreadFactory("MyThreadFactory");
        Thread thread;

        for (int i = 0; i < 5; i++) {
            thread = factoryTest.newThread(() -> {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }

        System.out.println("Factory stats:" + factoryTest.getStatus());
    }
}
