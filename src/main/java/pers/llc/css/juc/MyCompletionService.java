package pers.llc.css.juc;

import java.util.Random;
import java.util.concurrent.*;

/**
 * 启动10个线程，哪个先运行完就返回那个结果
 *
 * @author lls
 * @time 2018-09-30
 */
public class MyCompletionService {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        ExecutorService executor = Executors.newFixedThreadPool(10);
        CompletionService completionService = new ExecutorCompletionService(executor);
        for (int i = 1; i <= 10; i++) {
            final int result = i;
            completionService.submit(() -> {
                Thread.sleep(new Random().nextInt(3000));
                return result;
            });
        }
        System.out.println(completionService.take().get());
    }
}
