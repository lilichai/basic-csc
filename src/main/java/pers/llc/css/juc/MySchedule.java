package pers.llc.css.juc;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * schedule取代timer执行定时任务
 *
 * @author lls
 * @time 2018-10-02
 */
public class MySchedule {

    static ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(2);

    public static void main(String[] args) {

        scheduledExecutorService.scheduleWithFixedDelay(() -> System.out.println("start schedule"), 0, 1, TimeUnit.SECONDS);
        scheduledExecutorService.scheduleWithFixedDelay(() -> System.out.println("start schedule2"), 0, 2, TimeUnit.SECONDS);

    }
}
