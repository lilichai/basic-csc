package pers.llc.css.juc;


import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * CopyOnWriteArrayList会先复制一个集合副本，当对集合进行修改时普遍的上修改的是副本里的值，
 * 修改完后再将原因集合的引用指向这个副本，避免抛出ConcurrentModificationException异常
 * (具体分析见笔记)
 * 多线程可以同时对这个容器进行迭代，而不会干扰或者与修改容器的线程相互干扰
 *
 * @author lls
 * @time 2018-09-30
 */
public class MyCopyOnWriteArrayList {

    public static void main(String[] args) {

        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(3, 4, 3,
                TimeUnit.SECONDS, new ArrayBlockingQueue<>(3),
                new ThreadPoolExecutor.DiscardOldestPolicy());

//        Collection num = new ArrayList<String>();
        final Collection num = new CopyOnWriteArrayList();
        num.add("One");
        num.add("Two");
        num.add("Three");

        threadPool.execute(() -> {
            Iterator<String> iterator = num.iterator();
            while (iterator.hasNext()) {
                String element = iterator.next();
                if ("Two".equals(element)) {
                    num.remove(element);
                } else {
                    System.out.println(element);
                }
            }
        });

        threadPool.execute(() -> {
            Iterator<String> iterator = num.iterator();
            while (iterator.hasNext()) {
                String element = iterator.next();
                System.out.println(element + "ppp");
            }
        });
    }
}
