package pers.llc.css.thread;


/**
 * 进入Runnable（就绪）状态，不会释放锁
 *
 * @author lls
 * @time 2018-10-02
 */
public class MyYield {

    private static boolean ready;
    private static int number;

    private static class ReaderThread extends Thread {
        @Override
        public void run() {
            while (!ready) {
                Thread.yield();
            }
            System.out.println(number);
        }
    }

    public static void main(String[] args) {
        new ReaderThread().start();
        number = 42;
        ready = false;
    }
}
