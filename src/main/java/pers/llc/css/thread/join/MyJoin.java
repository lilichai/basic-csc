package pers.llc.css.thread.join;


import lombok.extern.slf4j.Slf4j;

/**
 * join会释放锁
 *
 * @author lls
 * @time 2018-09-30
 */
@Slf4j
public class MyJoin {

    static class ThreadA extends Thread {
        private ThreadB b;

        public ThreadA(ThreadB b) {
            this.b = b;
        }

        @Override
        public void run() {
            synchronized (b) {
                try {
                    b.start();
                    b.join(6000);
                    System.out.println("是否要等到B线程执行完");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class ThreadB extends Thread {
        @Override
        public void run() {
            try {
                System.out.println("b run begin timer= " + System.currentTimeMillis());
                Thread.sleep(3000);
                System.out.println("b run end timer=  " + System.currentTimeMillis());
                Thread.sleep(3000);
                System.out.println("是否释放");
                System.out.println("是否释放");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        synchronized public void bService() {
            System.out.println("打印了bservice timer=  " + System.currentTimeMillis());
        }
    }

    static class ThreadC extends Thread {
        private ThreadB threadB;

        public ThreadC(ThreadB threadB) {
            this.threadB = threadB;
        }

        @Override
        public void run() {
            threadB.bService();
        }
    }

    public static void main(String[] args) {
        log.info("logger ....");
        ThreadB b = new ThreadB();

        ThreadA a = new ThreadA(b);
        a.start();

        ThreadC c = new ThreadC(b);
        c.start();   //threadB.bService();方法能被立即调用，说明join方法具有释放锁的特点
    }
}
