package pers.llc.css.nio;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;


/**
 * FileChannel 从文件中读写数据。
 *
 * DatagramChannel 能通过UDP读写网络中的数据。
 *
 * SocketChannel 能通过TCP读写网络中的数据。
 *
 * ServerSocketChannel可以监听新进来的TCP连接，像Web服务器那样。对每一个新进来的连接都会创建一个SocketChannel。
 *
 * https://www.ibm.com/developerworks/cn/education/java/j-nio/j-nio.html
 *
 * @author fancy
 * @date: 2018/10/2 0002 21:02
 */
public class MyFileChannel {
    public static void main(String[] args) throws IOException {
        RandomAccessFile aFile = new RandomAccessFile("e:\\test.txt", "rw");
        FileChannel inChannel = aFile.getChannel();

        //涉及到的buffer的方法稍后解释
        ByteBuffer buf = ByteBuffer.allocate(48);

        int bytesRead = inChannel.read(buf);
        while (bytesRead != -1) {

            buf.flip();
            while (buf.hasRemaining()) {
                System.out.print((char) buf.get());
            }
            //buf.compact();也可以
            buf.clear();
            bytesRead = inChannel.read(buf);
        }
        aFile.close();
    }
}
