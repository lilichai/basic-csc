package pers.llc.css.nio.demo;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.util.Date;
import java.util.Scanner;

/**
 * @author fancy
 * @time 2019/3/31 0031 18:58
 */
public class DatagramClient {

    public static void main(String[] args) throws IOException {
        DatagramChannel dChannel = DatagramChannel.open();

        ByteBuffer buffer = ByteBuffer.allocate(1024);
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String string = scanner.next();
            buffer.put((new Date().toString() + ">>" + string).getBytes());
            buffer.flip();
            dChannel.send(buffer, new InetSocketAddress("127.0.0.1", 8989));
            buffer.clear();
        }
        dChannel.close();

    }
}
