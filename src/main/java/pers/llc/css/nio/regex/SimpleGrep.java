
package pers.llc.css.nio.regex;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * 查询文件中符种模式的字段
 * @author fancy
 */
public class SimpleGrep {

    private static final Pattern pattern = Pattern.compile("test");
    public static void main(String[] arg)
            throws Exception {

        test();
    }

    public static void test() throws IOException {

        Matcher matcher = pattern.matcher("");

        String file = "e:/test.txt";
        BufferedReader br = null;
        String line;

        try {
            br = new BufferedReader(new FileReader(file));
        } catch (IOException e) {

        }

        while ((line = br.readLine()) != null) {
            matcher.reset(line);
            if (matcher.find()) {
                System.out.println(file + ": " + line);
            }
        }
        br.close();
    }
}
