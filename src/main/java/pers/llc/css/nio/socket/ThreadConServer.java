package pers.llc.css.nio.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author fancy
 * @time 2018-10-01
 */
public class ThreadConServer {

    public void creatTask() throws IOException {
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(3, 4, 3,
                TimeUnit.SECONDS, new ArrayBlockingQueue<>(3),
                new ThreadPoolExecutor.DiscardOldestPolicy());

        ServerSocket ss = new ServerSocket(9999);
        while (!Thread.currentThread().isInterrupted()){
            Socket socket = ss.accept();
            threadPool.submit(new Task(socket));
        }
    }

    public static void main(String[] args) throws IOException {
      ThreadConServer server = new ThreadConServer();
      server.creatTask();
    }

    class Task extends Thread {
        private Socket socket;
        public Task(Socket socket){
            this.socket = socket;
        }
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted() && !socket.isClosed()){
                try {
                    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    String line = in.readLine();
                    if ("quit".equalsIgnoreCase(line)){
                        in.close();
                        out.close();
                        socket.close();
                        System.out.println("关闭服务器");
                        System.exit(0);
                    }
                    if (line != null){
                        System.out.println(Thread.currentThread().getName() + "客户端信息：" + line);
                        out.println("服务端接收成功!");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
