package pers.llc.css.excutor;

import java.util.concurrent.*;

/**
 * 执行完线程后返回结果
 *
 * @author lls
 */
public class CallableAndFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future = executor.submit(() -> "MOBIN");
        System.out.println("任务的执行结果：" + future.get());
    }
}
