package pers.llc.css.excutor;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 使用newScheduledThreadPool来模拟心跳机制
 *
 * @author lls
 */
public class HeartBeat {
    public static void main(String[] args) {

        ScheduledExecutorService executor = new ScheduledThreadPoolExecutor(5);
        Runnable task = () -> System.out.println("HeartBeat.........................");
        executor.scheduleAtFixedRate(task, 5, 3, TimeUnit.SECONDS);
    }
}
