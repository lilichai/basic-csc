package pers.llc.css.countDownLatch;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * @author author
 * @time 2018-09-30
 */
public class MyCountDownLatch {

    static CountDownLatch c = new CountDownLatch(2);

    public static void main(String[] args) throws InterruptedException {
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(3, 4, 3, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(3),
                new ThreadPoolExecutor.DiscardOldestPolicy());

        threadPool.execute(() -> {
            System.out.println(1);
            c.countDown();
            System.out.println(2);
            c.countDown();
        });
        //await会一直阻塞到计数器为零，或者等待中的线程中断，或者等待超时
        c.await();
        System.out.println(3);
    }
}
