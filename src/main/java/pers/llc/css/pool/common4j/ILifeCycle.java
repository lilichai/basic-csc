package pers.llc.css.pool.common4j;


/**
 * @author Fancy
 */
public interface ILifeCycle {

    void init();

    void destroy();
}

