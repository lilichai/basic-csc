package pers.llc.css.pool.util;

/**
 * 作用描述：
 *
 * @auther: Administrator
 * @date: 2018/10/19 0019 21:02
 */
public class SystemUtil {
    public SystemUtil() {
    }

    public static String getEndLine() {
        return System.getProperty("line.separator");
    }

    public static void main(String[] args) {
        getEndLine();
        getProcessorCount();
        System.out.println(getEndLine()+"ppp");
    }

    public static int getProcessorCount() {
        System.out.println(Runtime.getRuntime().availableProcessors());
        return Runtime.getRuntime().availableProcessors();
    }
}
