package pers.llc.css.pool.util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * 作用描述：
 *
 * @author Fancy
 * @date: 2018/10/22 0022 21:11
 */
public class XmlUtil {


    public static Element getTopElement(String xmlPath) {
        SAXReader reader = new SAXReader();
        Document document1 = null;
        try {
            document1 = reader.read(XmlUtil.class.getResource(xmlPath));
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        Element node = document1.getRootElement();
        return node;
    }

    public static void main(String[] args) {
        int i = 1;
        i = i++;
        int j = i++;
        int k = i + ++i * i++;
        System.out.println(i);
        System.out.println(j);
        System.out.println(k);

    }
}
