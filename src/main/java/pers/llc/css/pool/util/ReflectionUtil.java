package pers.llc.css.pool.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;

/**
 * 作用描述：
 *ReflectionThreadFactory
 * @auther: Administrator
 * @date: 2018/10/19 0019 21:01
 */
public class ReflectionUtil {
    private static final Logger _logger = LoggerFactory.getLogger(ReflectionUtil.class);

    private ReflectionUtil() {
    }

    public static <T> T createInstance(String className) {
        if (null == className) {
            return null;
        } else {
            Object listener = null;

            try {
                Class<T> listenerClass = (Class<T>) Class.forName(className);
                Constructor<T> constructor = listenerClass.getDeclaredConstructor();
                constructor.setAccessible(true);
                listener = constructor.newInstance();
            } catch (ClassNotFoundException var4) {
                _logger.error(String.format("could not found class:%s", className), var4);
            } catch (Exception var5) {
                _logger.error(String.format("create instance for class:%s fail", className), var5);
            }

            return (T) listener;
        }
    }

    public static <T> T createInstance(String className, Class<?>[] parameterTypes, Object[] initargs) {
        if (null == className) {
            return null;
        } else {
            Object listener = null;

            try {
                Class<T> listenerClass = (Class<T>) Class.forName(className);
                Constructor<T> constructor = listenerClass.getDeclaredConstructor(parameterTypes);
                constructor.setAccessible(true);
                listener = constructor.newInstance(initargs);
            } catch (ClassNotFoundException var6) {
                _logger.error(String.format("could not found class:%s", className), var6);
            } catch (Exception var7) {
                _logger.error(String.format("create instance for class:%s fail", className), var7);
            }

            return (T) listener;
        }
    }
}
