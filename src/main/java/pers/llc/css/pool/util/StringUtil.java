package pers.llc.css.pool.util;

/**
 * 作用描述：
 *
 * @auther: Administrator
 * @date: 2018/10/19 0019 21:02
 */
public class StringUtil {
    public StringUtil() {
    }

    public static boolean isNull(String str) {
        return null == str;
    }

    public static boolean isEmpty(String str) {
        return null == str || 0 == str.length();
    }

    public static boolean isBlank(String str) {
        if (isEmpty(str)) {
            return true;
        } else {
            for(int i = 0; i < str.length(); ++i) {
                if (!Character.isWhitespace(str.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
    }
}
