package pers.llc.css.lock.volatiles;

/**
 * volatile不保证原子性
 *
 * @author lls
 * @time 2018-04-07
 */
public class NoAtoVolatile {

    private static volatile int value;

    public synchronized void increment() {
        value++;
        System.out.println(value);
    }

    public void increment2() {
        value++;
        System.out.println(value);
    }

    public static void main(String[] args) {
        final NoAtoVolatile volatile2 = new NoAtoVolatile();
        for (int i = 0; i < 10; i++) {
            new Thread(() -> volatile2.increment()).start();
        }
    }
}
