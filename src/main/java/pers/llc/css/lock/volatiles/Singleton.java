package pers.llc.css.lock.volatiles;

/**
 * 不加volatile这种双重检测机制在JDK1.5之前是有问题的.
 * JDK1.5之后，可以使用volatile关键字修饰变量来解决无序写入产生的问题。
 * <p>
 * volatile关键字的一个重要作用是禁止指令重排序，即保证不会出现内存分配、
 * 返回对象引用、初始化这样的顺序，从而使得双重检测真正发挥作用。
 * 当然，也可以选择不使用双重检测，而采用非延迟加载的方式来达到相同的效果(static)
 * <p>
 * 单例模式的线程安全问题
 *
 * @author lls
 * @time 2019/4/7 0007 8:48
 */
public class Singleton {

    private volatile static Singleton singleton;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (singleton == null) {
            synchronized (Singleton.class) {
                if (singleton == null) {
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }
}
