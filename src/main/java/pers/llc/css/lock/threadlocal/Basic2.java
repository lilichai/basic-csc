package pers.llc.css.lock.threadlocal;

import java.util.Random;

/**
 * 一个ThreadLocal代表一个变量，所以其中只能放一个数据，当你有多个变量需要共享时就应该将定义一
 * 个对象来包装这些变量再在ThreadLocal对象中存储这个对象
 *
 * @author lls
 * @time 2018-10-02
 */
public class Basic2 {

    public static  void main(String[] args){
        for (int i = 0 ; i < 2; i ++) {
            new Thread(() -> {
                int data = new Random().nextInt();
                Person myThreadLocal =  Person.getInstance();
                System.out.println("线程:"+Thread.currentThread().getName()+"data  "+data);
                myThreadLocal.setName("mobin"+ data);
                myThreadLocal.setAge(data);
                new A().get();
                new B().get();
            }).start();
        }
    }
    static class  A{
        public void get(){
            Person AThreadLocal = Person.getInstance();
            System.out.println("A线程"+Thread.currentThread().getName()+"的name"+AThreadLocal.getName()+"    age"+AThreadLocal.getAge());
        }
    }

    static  class B{
        public void get(){
            Person BThreadLocal = Person.getInstance();
            System.out.println("B线程"+Thread.currentThread().getName()+"的name"+BThreadLocal.getName()+"    age"+BThreadLocal.getAge());
        }
    }

    static  class Person {
        private Person(){}//禁止外部直接创建对象
        private static  ThreadLocal<Person> mapThreadLocal = new ThreadLocal<>();

        private static Person getInstance(){
            Person myThreadLocal = mapThreadLocal.get();
            if(myThreadLocal == null){
                myThreadLocal = new Person();
                mapThreadLocal.set(myThreadLocal);
            }
               return myThreadLocal;
        }

        private String name;
        private int age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }
}
