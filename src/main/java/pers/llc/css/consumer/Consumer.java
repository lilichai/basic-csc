package pers.llc.css.consumer;


/**
 * @author lls
 * @time 2018-09-30
 */
public class Consumer implements Runnable{
    private EventStorage storage;
    public Consumer(EventStorage storage){
        this.storage = storage;
    }
    @Override
    public void run() {
        for(int i = 0; i < 100; i ++){
            storage.get();
        }
    }
}
