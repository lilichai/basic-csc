package pers.llc.css.pattern.ste.d1;

public class Main {
    public static void main(String[] args) {
        System.out.println("Testing Gate, hit CTRL+C to exit.");
        Gate gate = new Gate();
        new UserThread(gate, "A", "A").start();
        new UserThread(gate, "B", "B").start();
        new UserThread(gate, "C", "C").start();
    }
}
