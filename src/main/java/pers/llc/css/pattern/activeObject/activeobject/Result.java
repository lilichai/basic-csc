package pers.llc.css.pattern.activeObject.activeobject;

public abstract class Result<T> {
    public abstract T getResultValue(); //
}
