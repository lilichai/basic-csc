package pers.llc.css.pattern.future;

public interface Data {
    String getContent();
}
