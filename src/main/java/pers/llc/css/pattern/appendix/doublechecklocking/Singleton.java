package pers.llc.css.pattern.appendix.doublechecklocking;

import java.util.Date;

/**
 * 双重检查锁
 * http://www.cnblogs.com/xz816111/p/8470048.html
 *
 * @author fancy
 */
public class Singleton {

    private volatile static Singleton instance = null;
    private Date date = new Date();

    private Singleton() {
    }

    public Date getDate() {
        return date;
    }

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
