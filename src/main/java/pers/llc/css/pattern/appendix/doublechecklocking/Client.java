package pers.llc.css.pattern.appendix.doublechecklocking;

/**
 *
 * @author fancy
 */
public class Client {

    public static void main(String[] args) {
        // 线程A
        new Thread(() -> System.out.println(Singleton.getInstance().getDate())).start();

        // 线程B
        new Thread(() -> System.out.println(Singleton.getInstance().getDate())).start();
    }
}
